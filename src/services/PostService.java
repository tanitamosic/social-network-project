package services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonIOException;

import model.Comment;
import model.Post;
import repositories.CommentRepository;
import repositories.PostRepository;
import repositories.UserRepository;

public class PostService {
	
	private PostRepository postRepository;
	private UserRepository userRepository;
	private CommentRepository comRepository;
	
	public PostService(PostRepository pr, UserRepository ur, CommentRepository cr) {
		postRepository = pr;
		userRepository = ur;
		comRepository = cr;
	}
	
	public Post getPostById(Integer id) {
		return postRepository.getById(id);
	}
	
	public List<Post> getPostsByUser(Integer id){
		List<Post> posts = postRepository.getByPoster(id);
		posts.sort(null);
		instantiateFeedPosters(posts);
		instantiateFeedComments(posts);
		instantiateCommentUsers(posts);
		return posts;
	}
	
	public List<Post> getFeedForUser(Integer id){
		List<Post> feed = new ArrayList<Post>();
		List<Integer> friendIds = userRepository.getFriendsIds(id);
		
		feed.addAll(postRepository.getByPoster(id));
		for(Integer friend : friendIds) {
			feed.addAll(postRepository.getByPoster(friend));
		}
		
		feed.sort(null);
		instantiateFeedPosters(feed);
		instantiateFeedComments(feed);
		instantiateCommentUsers(feed);
		return feed;
	}
	
	private void instantiateCommentUsers(List<Post> posts) {
		for (Post p: posts) {
			for (Comment c: p.getComments()) {
				c.setPoster(userRepository.getById(c.getPosterId()));
			}
		}
	}

	public void instantiateFeedPosters(List<Post> posts) {
		for (Post p: posts) {
			p.setPoster(userRepository.getById(p.getPosterId()));
		}
	}
	public void instantiateFeedComments(List<Post> posts) {
		for (Post p: posts) {
			List<Comment> comments = new ArrayList<Comment>();
			for (Integer c: p.getCommentsIds()) {
				comments.add(comRepository.getById(c));
			}
			p.setComments(comments);
		}
	}
	
	public Post postPost(Integer posterId, String picture, String description) throws JsonIOException, IOException {
		Post newPost = new Post();
		newPost.setId(postRepository.getNewId());
		newPost.setPicture(picture);
		newPost.setDescription(description);
		newPost.setPoster(null); // !IMPORTANT
		newPost.setPosterId(posterId);
		newPost.setDate(new Date());
		newPost.setIsDeleted(false);
		newPost.setCommentsIds(new ArrayList<Integer>());
		
		postRepository.createPost(newPost);
		
		return newPost;
	}
	
	public void deletePost(Integer postId) throws JsonIOException, IOException {
		postRepository.deletePost(postId);
	}
	
	

}