package services;

import java.util.HashMap;

import repositories.UserRepository;
import ws.WsHandler;

public class ChatService {
	
	private UserRepository ur;
	private WsHandler handler;
	
	public ChatService(UserRepository ur) {
		this.ur = ur;
	}
	
	public Long getChatPath(Integer user1, Integer user2) {
		String username1 = ur.getById(user1).getUsername();
		String username2 = ur.getById(user2).getUsername();
		
		if(user1>user2) 
			return hash(username1+username2);
		else
			return hash(username2+username1);
	}
	
	private Long hash(String text) {

	    Long hash = Long.parseLong("17");
	    for (int i = 0; i < text.length(); i++) {
	        hash = hash*31 + text.charAt(i);
	    }

	    return Math.abs(hash);
	}
}
