package services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonIOException;

import model.Comment;
import model.Picture;
import model.Post;
import model.User;
import repositories.CommentRepository;
import repositories.PictureRepository;
import repositories.UserRepository;

public class PictureService {
	
	private PictureRepository repo;
	private CommentRepository com;
	private UserRepository ur;
	
	public PictureService(PictureRepository pr, UserRepository ur, CommentRepository com) {
		
		this.repo = pr;
		this.ur = ur;
		this.com = com;
	}
	
	public List<Picture> getPictures(Integer id) {
		List<Picture> retval = repo.getByPoster(id);
		instantiateCommentUsers(retval);
		instantiateFeedPosters(retval);
		instantiateFeedComments(retval);
		return retval;
		
	}

	public Picture postPicture(Integer posterId, String picture, String description) throws JsonIOException, IOException {
		Picture newPicture = new Picture();
		newPicture.setId(repo.getNewId());
		newPicture.setPicture(picture);
		newPicture.setDescription(description);
		newPicture.setPoster(ur.getById(posterId));
		newPicture.setPosterId(posterId);
		newPicture.setDate(new Date());
		newPicture.setIsDeleted(false);
		
		repo.createPicture(newPicture);
		
		return newPicture;
	}
	
	public void deletePicture(Integer pictureId) throws JsonIOException, IOException {
		repo.deletePicture(pictureId);
	}
	
	private void instantiateCommentUsers(List<Picture> posts) {
		for (Picture p: posts) {
			if (null == p.getComments()) {
				p.setComments(new ArrayList<Comment>());
			}
			for (Integer c: p.getCommentsIds()) {
				Comment e = com.getById(c);
				User poster = ur.getById(e.getPosterId());
				e.setPoster(poster);
				p.getComments().add(e);
			}
		}
	}

	public void instantiateFeedPosters(List<Picture> posts) {
		for (Picture p: posts) {
			p.setPoster(ur.getById(p.getPosterId()));
		}
	}
	public void instantiateFeedComments(List<Picture> posts) {
		for (Picture p: posts) {
			List<Comment> comments = new ArrayList<Comment>();
			for (Integer c: p.getCommentsIds()) {
				comments.add(com.getById(c));
			}
			p.setComments(comments);
		}
	}
	

}