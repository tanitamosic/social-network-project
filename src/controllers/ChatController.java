package controllers;

import java.util.HashMap;
import java.util.List;

import model.User;
import model.enums.Role;
import services.ChatService;
import spark.Spark;
import util.Authorizer;
import ws.WsHandler;

public class ChatController {
	
	private ChatService cs;
	private Authorizer auth;
	
	public ChatController(ChatService cs, Authorizer auth) {
		this.cs = cs;
		this.auth = auth;
		getPath();
		sendMessage();
	}

	private void getPath() {

		Spark.get("/user/chat/getpath/:userid/:friendid", (req,res)->{
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer userid = Integer.valueOf(req.params("userid"));
				Integer friendid = Integer.valueOf(req.params("friendid"));
				return cs.getChatPath(userid, friendid);
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad Request\"}";
			}
		});
		
	}

	private void sendMessage() {
		
		
	}
	
	

}
