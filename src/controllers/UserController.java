package controllers;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import com.google.gson.Gson;

import model.User;
import model.enums.Gender;
import model.enums.Role;
import services.UserService;
import spark.Session;
import spark.Spark;
import util.Authorizer;

public class UserController {

	private UserService us;
	private Gson gson;
	private Authorizer auth;

	public UserController(UserService ps, Gson gson, Authorizer a) {
		this.us  = ps;
		this.gson = gson;
		this.auth = a;
		
		login();
		register();
		getFriends();
		banUser();
		blockUser();
		changePassword();
		changeProfilePicture();
		getBlockedUsers();
		unblockUser();
		removeFriend();
		getUser();
		privateUser();
		logout();
		getMutualFriends();
	}

	@SuppressWarnings("unused")
	private class UserWithToken {
		public User user;
		public String token;
	}
	@SuppressWarnings("unused")
	private class LoginCredentials {
		private String emailOrUsername = null;
		private String password = null;
		
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEmailOrUsername() {
			return emailOrUsername;
		}
		public void setEmailOrUsername(String emailOrUsername) {
			this.emailOrUsername = emailOrUsername;
		}
	}
	
	private void login() {
		Spark.post("/login", (req, res) ->{
			res.type("application/json");
			String s = req.body();
			Gson gson = new Gson();
			LoginCredentials cred = gson.fromJson(s, LoginCredentials.class);
			User u = us.attemptLogin(cred.getEmailOrUsername(), cred.getPassword());
			Session sess = req.session(true);
			User user = sess.attribute("user");
			if (u==null) {
				res.status(400);
				return "{\"message\": \"Invalid username or password\"}";
			}
			if (user == null) {
				user = u;
				sess.attribute("user",user);
			}
			String token = auth.createJWTToken(u, req);
			UserWithToken uwt = new UserWithToken();
			uwt.user = u;
			uwt.token = token;
			return gson.toJson(uwt);
		});
	}
	
	private void logout() {
		Spark.get("/logout/:userid", (req, res) ->{
			res.type("application/json");
			Session sess = req.session(true);
			User user = sess.attribute("user");
			
			if(user != null) {
				sess.invalidate();
			}
			
			return true;
		});
		
	}
	
	@SuppressWarnings("unused")
	private class RegistrationRequest {
		private String username;
		private String password1;
		private String password2;
		private String name;
		private String surname;
		private Date dateOfBirth;
		private String email;
		private Gender gender;
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword1() {
			return password1;
		}
		public void setPassword1(String password1) {
			this.password1 = password1;
		}
		public String getPassword2() {
			return password2;
		}
		public void setPassword2(String password2) {
			this.password2 = password2;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getSurname() {
			return surname;
		}
		public void setSurname(String surname) {
			this.surname = surname;
		}
		public Date getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Gender getGender() {
			return gender;
		}
		public void setGender(Gender gender) {
			this.gender = gender;
		}
		
	}
	
	private void register() {
		Spark.post("/register", (req, res) ->{
			res.type("application/json");
			String s = req.body();
			Gson gson = new Gson();
			
			RegistrationRequest regRequest = gson.fromJson(s, RegistrationRequest.class);
			boolean success1 = us.checkUniqueCredentials(regRequest.getEmail(), regRequest.getUsername());
			boolean success2 = us.checkNewPassword(regRequest.getPassword1(), regRequest.getPassword2());
			
			if (success1 && success2) {
				us.registerUser(regRequest.getUsername(),
						regRequest.getPassword1(), regRequest.getEmail(), 
						regRequest.getName(), regRequest.getSurname(),
						regRequest.getDateOfBirth(), regRequest.getGender());
			}
			
			return "{\"message\": \"Registration successful!\"}";
		});
	}

	private void getFriends() {
		Spark.get("/user/friends/:userid", (req, res) -> {
			res.type("application/json");
			
			try {
				Integer userid = Integer.valueOf(req.params("userid"));
				List<User> friends = us.getFriends(userid);
				return gson.toJson(friends);
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad Request\"}";
			}
			
		});
	}
	
	private void banUser() {
		Spark.delete("/user/ban/:usrid", (req, res) -> {
			res.type("application/json");
			if (!auth.checkJWTToken(req, Role.ADMIN)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.params("usrid"));
				us.suspendUser(userid);
				return "{\"message\": \"Suspension successful!\"}";
			} catch (Exception e) { 
				return "{\"message\": \"Suspension failed!!\"}";
			}
			
		});
	}
	
	private void blockUser() {
		Spark.post("/user/block", (req, res) -> {
			res.type("application/json");
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.queryParams("user"));
				Integer blockid = Integer.valueOf(req.queryParams("person"));
				us.blockPerson(userid, blockid);
				return "{\"message\": \"You've successfully blocked this person!\"}";
			} catch (Exception e) { 
				System.out.println(e.getMessage());
				return "{\"message\": \"Bad request!!\"}";
			}
		});
	}
	
	@SuppressWarnings("unused")
	private class NewPassword {
		
		private Integer userid;
		private String oldPassword;
		private String newPassword1;
		private String newPassword2;
		public Integer getUserid() {
			return userid;
		}
		
		public void setUserid(Integer userid) {
			this.userid = userid;
		}
		public String getOldPassword() {
			return oldPassword;
		}
		public void setOldPassword(String oldPassword) {
			this.oldPassword = oldPassword;
		}
		public String getNewPassword1() {
			return newPassword1;
		}
		public void setNewPassword1(String newPassword1) {
			this.newPassword1 = newPassword1;
		}
		public String getNewPassword2() {
			return newPassword2;
		}
		public void setNewPassword2(String newPassword2) {
			this.newPassword2 = newPassword2;
		}
		
	}
	
	private void changePassword() {
		Spark.post("/user/changepassword", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				NewPassword newpassword = gson.fromJson(req.body(), NewPassword.class);
				us.changePassword(newpassword.getUserid(), newpassword.getOldPassword(),
						newpassword.getNewPassword1(), newpassword.getNewPassword2());
				return "{\"message\": \"Password updated!\"}";
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Password update failed!!\"}";
			}
		});
	}

	private void changeProfilePicture() {
		Spark.post("/upload/avatar", (req, res) -> {
			
			res.type("application/json");
            
            if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
            
			try {
				String filename = generateRandomFileName();
				String absolutePath = new File("./view/images/avatars/").getAbsolutePath();
				String httplink = "http://localhost:3000/images/avatars/" + filename;
				MultipartConfigElement multipartConfigElement = new MultipartConfigElement(absolutePath);
	            req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
				
	            Part file = req.raw().getPart("file");
	            file.write(filename);
	            Integer userid = Integer.valueOf(req.queryParams("userid"));
	            User updatedUser = us.changeProfilePicture(userid, httplink);
	            return gson.toJson(updatedUser);
			} catch (Exception e) {
				res.status(400);
				return "";
			}
		});
	}
	private String generateRandomFileName() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 18;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString() + ".jpg";
    }

	private void getBlockedUsers() {
		Spark.get("/user/blocked/:userid", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.params("userid"));
				List<User> blocked = us.getBlockedUsers(userid);
				return gson.toJson(blocked);
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad Request\"}";
			}
		});
	}
	
	private void unblockUser() {
		Spark.get("/user/unblock", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.queryParams("user"));
				Integer blockedid = Integer.valueOf(req.queryParams("person"));
				us.unblockPerson(userid, blockedid);
				return "{\"message\": \"You've successfully unblocked this person! You can be friends again!\"}";
			} catch (Exception e) { 
				System.out.println(e.getMessage());
				return "{\"message\": \"Bad request!!\"}";
			}
		});
		
	}
	
	private void removeFriend() {
		Spark.get("/user/unfriend", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.queryParams("user"));
				Integer friend = Integer.valueOf(req.queryParams("friend"));
				us.removeFriend(userid, friend);
				return "{\"message\": \"You've successfully unfriended this person. To become friends again send a friend request.\"}";
			} catch (Exception e) { 
				System.out.println(e.getMessage());
				return "{\"message\": \"Bad request!!\"}";
			}
		});
	}
	
	private void getUser() {
		Spark.get("/user/getperson/:personid", (req, res) -> {
			res.type("application/json");
			
			try {
				Integer personid = Integer.valueOf(req.params("personid"));
				User person = us.getUser(personid);
				return gson.toJson(person);
			} catch (Exception e) { 
				System.out.println(e.getMessage());
				return "{\"message\": \"Bad request!!\"}";
			}
		});
		
	}
	
	private void privateUser() {
		Spark.get("/user/private", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.queryParams("userid"));
				Boolean privated = Boolean.valueOf(req.queryParams("privated"));
				us.togglePrivate(userid, privated);
				return "{\"message\": \"Your profile is now private!\"}";
			} catch (Exception e) { 
				System.out.println(e.getMessage());
				return "{\"message\": \"Bad request!!\"}";
			}
		});
	}
	
	private void getMutualFriends() {
		Spark.get("/user/:user/get-mutual-friends/:friend", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			try {
				Integer userid = Integer.valueOf(req.params("user"));
				Integer friend = Integer.valueOf(req.params("friend"));
				
				return gson.toJson(us.getMutualFriends(userid, friend));
			} catch (Exception e) { 
				System.out.println(e.getMessage());
				return "{\"message\": \"Bad request!!\"}";
			}
		});
	}
}
