package controllers;

import java.util.List;

import com.google.gson.Gson;

import model.Picture;
import model.Post;
import model.enums.Role;
import services.PictureService;
import spark.Spark;
import util.Authorizer;

public class PictureController {
	
	private PictureService ps;
	private Gson gson;
	private Authorizer auth;
	
	public PictureController(PictureService ps, Gson gson, Authorizer a) {
		this.ps  = ps;
		this.gson = gson;
		this.auth = a;
		postPicture();
		getMyGallery();
		deletePicture();
	}
	
	@SuppressWarnings("unused")
	private class PictureInfo {
		private Integer posterId;
		private String picture;
		private String description;

		public String getPicture() {
			return picture;
		}
		public void setPicture(String picture) {
			this.picture = picture;
		}
		public Integer getPosterId() {
			return posterId;
		}
		public void setPosterId(Integer posterId) {
			this.posterId = posterId;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
		
	}
	
	private void postPicture() {
		Spark.post("/picture/post", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				String picture = req.body();
				PictureInfo ci = gson.fromJson(picture, PictureInfo.class);
				Picture c = ps.postPicture(ci.getPosterId(), ci.getPicture(), ci.getDescription());
				
				return gson.toJson(c);
			} catch (Exception e) {
				System.out.println(e.getStackTrace().toString());
				res.status(400);
				return "{}";
			}
		});
	}
	public void getMyGallery() {
		Spark.get("/user/gallery/pictures", (req, res) ->{
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer id = Integer.valueOf(req.queryParams("userid"));
				List<Picture> posts = ps.getPictures(id);
				return gson.toJson(posts);
			} catch (NumberFormatException e) {
				res.status(400);
				return "{\"message\":\"Bad Request\"}";
			}
		});
	}
	
	public void deletePicture() {
		Spark.delete("/delete/picture/:pictureid", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer postid = Integer.valueOf(req.params("pictureid"));
				ps.deletePicture(postid);
				return "{\"message\":\"Deletion successful!\"}";
			} catch (Exception e) {
				res.status(400);
				return "{\"message\":\"Deletion failed!!\"}";
			}
		});
	}
}
