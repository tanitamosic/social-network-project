package controllers;

import java.io.File;
import java.util.List;
import java.util.Random;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import com.google.gson.Gson;

import model.Post;
import model.enums.Role;
import services.PostService;
import spark.Spark;
import util.Authorizer;

public class PostController {

	private PostService ps;
	private Gson gson;
	private Authorizer auth;

	public PostController(PostService ps, Gson gson, Authorizer a) {
		this.ps  = ps;
		this.gson = gson;
		this.auth = a;
		
		getFeed();
		getMyPosts();
		postPost();
		deletePost();
		uploadImage();
	}
	
	public void getFeed() {
		Spark.get("/user/myfeed/posts", (req, res) ->{
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer id = Integer.valueOf(req.queryParams("userid"));
				List<Post> posts = ps.getFeedForUser(id);
				return gson.toJson(posts);
			} catch (NumberFormatException e) {
				res.status(400);
				return "{\"message\":\"Bad Request\"}";
			}
		});
	}
	
	public void getMyPosts() {
		Spark.get("/user/myposts/posts", (req, res) ->{
			res.type("application/json");
			
			try {
				Integer id = Integer.valueOf(req.queryParams("userid"));
				List<Post> posts = ps.getPostsByUser(id);
				return gson.toJson(posts);
			} catch (NumberFormatException e) {
				res.status(400);
				return "{\"message\":\"Bad Request\"}";
			}
		});
	}
	
	public void deletePost() {
		Spark.delete("/delete/post/:postid", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer postid = Integer.valueOf(req.params("postid"));
				ps.deletePost(postid);
				return "{\"message\":\"Deletion successful!\"}";
			} catch (Exception e) {
				res.status(400);
				return "{\"message\":\"Deletion failed!!\"}";
			}
		});
	}
	
	@SuppressWarnings("unused")
	private class PostInfo {
		private Integer posterId;
		private String picture;
		private String description;

		public String getPicture() {
			return picture;
		}
		public void setPicture(String picture) {
			this.picture = picture;
		}
		public Integer getPosterId() {
			return posterId;
		}
		public void setPosterId(Integer posterId) {
			this.posterId = posterId;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
		
	}
	
	private void postPost() {
		Spark.post("/post/post", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				String picture = req.body();
				PostInfo ci = gson.fromJson(picture, PostInfo.class);
				Post c = ps.postPost(ci.getPosterId(), ci.getPicture(), ci.getDescription());
				
				return gson.toJson(c);
			} catch (Exception e) {
				System.out.println(e.getStackTrace().toString());
				res.status(400);
				return "{}";
			}
		});
	}
	
	private void uploadImage() {
		Spark.post("/upload/image", "multipart/form-data", (req, res) -> {
			
			
            res.type("application/json");
            
            if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
            
			try {
				String filename = generateRandomFileName();
				String absolutePath = new File("./view/images/postImages/").getAbsolutePath();
				String httplink = "http://localhost:3000/images/postImages/" + filename;
				MultipartConfigElement multipartConfigElement = new MultipartConfigElement(absolutePath);
	            req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
				
	            Part file = req.raw().getPart("file");
	            file.write(filename);
	            
				return "{\"imgpath\": \"" + httplink + "\"}";
			} catch (Exception e) {
				res.status(400);
				return "";
			}
		});
	}
	
	private String generateRandomFileName() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 18;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString() + ".jpg";
    }

}
