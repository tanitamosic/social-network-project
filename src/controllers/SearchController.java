package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;

import model.User;
import services.SearchService;
import spark.Spark;

public class SearchController {

	private SearchService ss;
	private Gson gson;

	public SearchController(SearchService ss, Gson gson) {
		this.ss = ss;
		this.gson = gson;
		
		initiateSearch();
		searchByEmail();
	}

	private void searchByEmail() {
		Spark.get("/admin/search-by-email/:query", (req,res) -> {
			res.type("application/json");
			try {
				String email = req.queryParams("searchByEmail");
				List<User> result = ss.searchByEmail(email); 
				return gson.toJson(result);
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad request!\"}";
			}

		});
	}

	private void initiateSearch() {
		Spark.get("/user/search", (req, res) -> {
			res.type("application/json");
			
			try {
				String name = req.queryParams("name");
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String minDateString = req.queryParams("minDate");
				String maxDateString = req.queryParams("maxDate");
				Date minDate = null;
				Date maxDate = null;
				if(!minDateString.isBlank()) {
				 minDate = formatter.parse(minDateString);
				}
				if(!maxDateString.isBlank()) {
				 maxDate = formatter.parse(minDateString);
				}
				
				List<User> result = ss.combineSearch(name, new Date[] {minDate, maxDate});
				return gson.toJson(result);
				
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad request!\"}";
			}

		});
		
	}
	
	

}
