package repositories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonIOException;

import model.Picture;
import util.DataHandler;

public class PictureRepository {
	
	private Map<Integer, Picture> pictures;
	
	public PictureRepository() throws FileNotFoundException {
		pictures = new HashMap<Integer,Picture>();
		
		for (Picture c : DataHandler.loadPictures()) {
			pictures.put(c.getId(), c);
		}
	}
	
	public Picture getById(Integer id) {
		return pictures.get(id);
	}
	
	public List<Picture> getByPoster(Integer id){
		List<Picture> res = new ArrayList<Picture>();
		for(Picture p : pictures.values()) {
			if (p.getPosterId().equals(id) && !p.getIsDeleted()) res.add(p);
		}
		return res;
	}
	
	public Integer getNewId() {
		return pictures.size() + 1;
	}
	
	public void createPicture(Picture picture) throws JsonIOException, IOException {
		pictures.put(picture.getId(), picture);
		DataHandler.writePictures(new ArrayList<Picture>(pictures.values()));
	}

	public void deletePicture(Integer pictureId) throws JsonIOException, IOException {
		pictures.get(pictureId).setIsDeleted(true);
		DataHandler.writePictures(new ArrayList<Picture>(pictures.values()));	
	}

}
