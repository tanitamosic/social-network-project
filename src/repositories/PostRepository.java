package repositories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonIOException;

import model.Post;
import util.DataHandler;

public class PostRepository {
	
	private Map<Integer, Post> posts;
	
	public PostRepository() throws FileNotFoundException {
		posts = new HashMap<Integer,Post>();
		
		for (Post c : DataHandler.loadPosts()) {
			posts.put(c.getId(), c);
		}
	}
	
	public Post getById(Integer id) {
		return posts.get(id);
	}
	
	public List<Post> getByPoster(Integer id){
		List<Post> res = new ArrayList<Post>();
		for(Post p : posts.values()) {
			if (p.getPosterId().equals(id) && p.getIsDeleted() == false) res.add(p);
		}
		return res;
	}
	
	public void updatePosts() throws JsonIOException, IOException {
		DataHandler.writePosts(new ArrayList<Post>(posts.values()));
	}

	public Integer getNewId() {
		return posts.size() + 1;
	}
	
	public void createPost(Post post) throws JsonIOException, IOException {
		posts.put(post.getId(), post);
		updatePosts();
	}
	
	public void deletePost(Integer postId) throws JsonIOException, IOException {
		posts.get(postId).setIsDeleted(true);
		updatePosts();
	}
}
