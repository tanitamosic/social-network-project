package repositories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonIOException;

import model.Comment;
import util.DataHandler;

public class CommentRepository {
	
	private Map<Integer, Comment> comments;
	
	public CommentRepository() throws FileNotFoundException {
		comments = new HashMap<Integer,Comment>();
		
		for (Comment c : DataHandler.loadComments()) {
			comments.put(c.getId(), c);
		}
	}
	
	public Comment getById(Integer id) {
		return comments.get(id);
	}
	
	public void createComment(Comment comment) throws JsonIOException, IOException {
		comments.put(comment.getId(), comment);
		DataHandler.addComment(comment);
		
	}
	public Integer getNewId() {
		return comments.size() + 1;
	}
	
	public void deleteComment(Integer comid) throws JsonIOException, IOException {
		comments.get(comid).setDeleted(true);
		updateComments();
	}
	
	public void updateComments() throws JsonIOException, IOException {
		DataHandler.writeComments(new ArrayList<Comment>(comments.values()));
	}
	
}
