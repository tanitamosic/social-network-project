package util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

import model.Comment;
import model.FriendRequest;
import model.Picture;
import model.Post;
import model.User;

public class DataHandler {
	
	private static String UsersPath = "./data/userdata/users.json";
	private static String CommentsPath = "./data/postdata/comments.json";
	private static String FriendRequestsPath = "./data/userdata/friendrequests.json";
	private static String PicturesPath = "./data/postdata/pictures.json";
	private static String PostsPath = "./data/postdata/posts.json";
	private static Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
	
	public static void writeUsers(List<User> gus) throws JsonIOException, IOException {
		Writer w = new FileWriter(UsersPath);
		g.toJson(gus, w);
		w.flush();
		w.close();
	}
	
	public static List<User> loadUsers() throws FileNotFoundException {
		Type t = new TypeToken<List<User>>() {}.getType();
		List<User> gus = g.fromJson(new FileReader(UsersPath), t);
		return gus;
	}
	
	public static void addUser(User gu) throws JsonIOException, IOException {
		List<User> users = loadUsers();
		users.add(gu);
		writeUsers(users);
	}
	
	public static void writeComments(List<Comment> gus) throws JsonIOException, IOException {
		Writer w = new FileWriter(CommentsPath);
		g.toJson(gus, w);
		w.flush();
		w.close();
	}
	
	public static List<Comment> loadComments() throws FileNotFoundException {
		Type t = new TypeToken<List<Comment>>() {}.getType();
		List<Comment> gus = g.fromJson(new FileReader(CommentsPath), t);
		return gus;
	}
	
	public static void addComment(Comment gu) throws JsonIOException, IOException {
		List<Comment> users = loadComments();
		users.add(gu);
		writeComments(users);
	}
	
	public static void writeFriendRequests(List<FriendRequest> gus) throws JsonIOException, IOException {
		Writer w = new FileWriter(FriendRequestsPath);
		g.toJson(gus, w);
		w.flush();
		w.close();
	}
	
	public static List<FriendRequest> loadFriendRequests() throws FileNotFoundException {
		Type t = new TypeToken<List<FriendRequest>>() {}.getType();
		List<FriendRequest> gus = g.fromJson(new FileReader(FriendRequestsPath), t);
		return gus;
	}
	
	public static void addFriendRequest(FriendRequest gu) throws JsonIOException, IOException {
		List<FriendRequest> users = loadFriendRequests();
		users.add(gu);
		writeFriendRequests(users);
	}
	
	public static void writePictures(List<Picture> gus) throws JsonIOException, IOException {
		Writer w = new FileWriter(PicturesPath);
		g.toJson(gus, w);
		w.flush();
		w.close();
	}
	
	public static List<Picture> loadPictures() throws FileNotFoundException {
		Type t = new TypeToken<List<Picture>>() {}.getType();
		List<Picture> gus = g.fromJson(new FileReader(PicturesPath), t);
		return gus;
	}
	
	public static void addPicture(Picture gu) throws JsonIOException, IOException {
		List<Picture> users = loadPictures();
		users.add(gu);
		writePictures(users);
	}
	
	public static void writePosts(List<Post> gus) throws JsonIOException, IOException {
		Writer w = new FileWriter(PostsPath);
		g.toJson(gus, w);
		w.flush();
		w.close();
	}
	
	public static List<Post> loadPosts() throws FileNotFoundException {
		Type t = new TypeToken<List<Post>>() {}.getType();
		List<Post> gus = g.fromJson(new FileReader(PostsPath), t);
		return gus;
	}
	
	public static void addPost(Post gu) throws JsonIOException, IOException {
		List<Post> users = loadPosts();
		users.add(gu);
		writePosts(users);
	}
	
}
