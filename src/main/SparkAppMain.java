package main;

import spark.Request;
import spark.Response;
import spark.Spark;
import util.Injector;

import java.io.File;
import java.io.IOException;

import ws.WsHandler;

public class SparkAppMain {
	
	public static void main(String[] args) throws IOException {
		Spark.port(8080);
		Spark.staticFiles.externalLocation(new File("./view").getCanonicalPath());
		
		Spark.webSocket("/ws/chat/t/*", WsHandler.class);
		Injector injector = new Injector();
		injector.initiate();
		
	}
	
	public static void checkJWTToken(Request req, Response res) {
		
		
	}

}
