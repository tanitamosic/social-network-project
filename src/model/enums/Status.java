package model.enums;

public enum Status {
	PENDING,
	ACCEPTED,
	DENIED
}
