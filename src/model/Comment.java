package model;

import java.util.Date;

import com.google.gson.annotations.Expose;

public class Comment {

	private Integer id;
	private Integer posterId; // user.id
	private Date date;
	private String comment;
	private Boolean edited;
	private Boolean deleted;
	
	@Expose (serialize = false, deserialize = false) 
	private User poster;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPosterId() {
		return posterId;
	}
	public void setPosterId(Integer poster) {
		this.posterId = poster;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Boolean getEdited() {
		return edited;
	}
	public void setEdited(Boolean edited) {
		this.edited = edited;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public User getPoster() {
		return poster;
	}
	public void setPoster(User poster) {
		this.poster = poster;
	}
	
	

	
	
}
