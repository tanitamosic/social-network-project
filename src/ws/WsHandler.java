package ws;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

@WebSocket
public class WsHandler extends WebSocketHandler {

	private static final Map<Long, Session[]> allSessions = new ConcurrentHashMap<Long, Session[]>();

	@Override
	public void configure(WebSocketServletFactory arg0) {
		arg0.getPolicy().setIdleTimeout(1000 * 60 * 60);
	}

	@OnWebSocketConnect
	public void connected(Session session) {

		Long hash = extractPathHash(session);
		String username = extractPathUsername(session);
		boolean hasSession = hasOpenSession(hash, username);
		
		if (hasSession) {
			replaceSession(session, hash, username);
		} else {
			placeSession(session, hash);
		}
	}
	
	private void replaceSession(Session newSession, Long hash, String username) {
		Session[] sessions = allSessions.get(hash);
		for (int i = 0; i< sessions.length; i++ ) {
			if (null == sessions[i]) {
				// skip
			} else if (username.equals(extractPathUsername(sessions[i]))) {
				sessions[i] = newSession;
			}
		}
	}
	
	private void placeSession(Session newSession, Long hash) {
		Session[] sessions = allSessions.get(hash);
		for (int i = 0; i< sessions.length; i++ ) {
			if (null == sessions[i]) {
				sessions[i] = newSession;
				break;
			}
		}
	}
	
	private boolean hasOpenSession(Long hash, String username) {
		if (null == allSessions.get(hash)) {
			allSessions.put(hash, new Session[] {null, null});
			return false;
		}
		Session[] sessions = allSessions.get(hash);
		for (int i = 0; i< sessions.length; i++ ) {
			if (null == sessions[i]) {
				// skip
			} else if (username.equals(extractPathUsername(sessions[i]))) {
				return true;
			}
		}
		return false;
	}

	private String extractPathUsername(Session session) {
		URI v1 = session.getUpgradeRequest().getRequestURI();
		String[] pathParts = v1.getPath().split("/");
		String username = pathParts[pathParts.length - 2];
		return username;
	}

	private Long extractPathHash(Session session) {
		URI v1 = session.getUpgradeRequest().getRequestURI();
		String[] pathParts = v1.getPath().split("/");
		Long hash = Long.parseLong(pathParts[pathParts.length - 1]);
		return hash;
	}
	
	private void removeSession(Long hash, String username) {
		Session[] sessions = allSessions.get(hash);
		for (int i = 0; i< sessions.length; i++ ) {
			if (username.equals(extractPathUsername(sessions[i]))) {
				sessions[i] = null;
			}
		}
	}
	
	@OnWebSocketClose
	public void closed(Session s, int statusCode, String reason) {
		Long hash = extractPathHash(s);
		String username = extractPathUsername(s);
		removeSession(hash, username);
	}

	@OnWebSocketError
	public void error(Session s, Throwable t) {
		Long hash = extractPathHash(s);
		String username = extractPathUsername(s);
		removeSession(hash, username);
	}

	@OnWebSocketMessage
	public void message(Session session, String message) {
		System.out.println("Received: " + message);
		Long hash = extractPathHash(session);
		String username = extractPathUsername(session);
		boolean hasSession = hasOpenSession(hash, username);
		
		if (hasSession) {
			postMessage(message, session, hash);
		}
	}

	public static void postMessage(String text, Session session, Long hash) {
		Session[] sessions = allSessions.get(hash);
		for (int i = 0; i< sessions.length; i++ ) {
			try {
				if (null == sessions[i]) {
					continue;
				}
				if (sessions[i].hashCode() != session.hashCode()) {
					sessions[i].getRemote().sendString(text);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
