import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
const Vuesax = require('vuesax')

import 'vuesax/dist/vuesax.css' //Vuesax styles
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false
Vue.use(VueRouter);
Vue.use(Vuesax);

new Vue({
  router,
  Vuesax,
  vuetify,
  render: h => h(App)
}).$mount('#app');